function user(args, socket, users){
	// validação inicial dos parâmetros
	if ((typeof args[1] == "undefined") ||
	    (typeof args[2] == "undefined") ||
	    (typeof args[3] == "undefined") ||
	    (typeof args[4] == "undefined")) {
		socket.write("Parâmetros inválidos!\n");
		return;
	}
	
	if(users[ args[1]]) {
		socket.write("Username já utilizado!\n");
		return;
	}
	
	if (socket.username ) delete users[socket.username];
	socket.username = args[1];
	
	// valida se o parâmetro do modo é um número
	if(isNaN(args[2])){
		socket.write("Modo informado é inválido!\n");
		return;
	}	
	socket.mode = args[2];
	// validação e atribuição do nome completo.
	var nomeCompleto = args.join(" ");
	nomeCompleto = nomeCompleto.split(":")[1];
	if (typeof nomeCompleto == "undefined"){
		socket.write("Nome completo inválido!\n");
		return;
	}	
	socket.realname = nomeCompleto.trim();
	
	users[args[1]] = socket.name;
	socket.write("O usuário " + socket.name + " agora é " + socket.username + "\n");
	socket.write("Modo: " + socket.mode + "\nNome completo: " + socket.realname + "\n");
  }
module.exports = user;
