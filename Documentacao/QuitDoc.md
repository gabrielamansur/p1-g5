## QUIT
   
### SINTAXE
    
####Format:on <level>:QUIT:<commands>
####Example:on 1:QUIT:/notice $me $nick just quit IRC with the message $1-
####/quit <razão>

### UTILIZAÇÃO
    
 Faz com que você desconecte do servidor. Se vocês
 Incluir uma razão, ele será exibido em todos os canais
 Como você sair.

### CODIGO "QUIT"

package xgen.irc.commands;

import xgen.irc.*;

public class Quit extends Command {
    
    public Quit() {
        super( "QUIT" );
    }

    public void handle( Context context , Connection conn , String rest ) {
        conn.closed();
    }
}

quit.txt

#### Aluno Julio Cesar
